<?php
    include 'inc/env.php';
    require 'lib/encoding.class.php';
    require_once 'lib/random_compat/random.php';
    
    if(!defined('__TGWEBHOOKCALL__'))
    {
        header('HTTP/1.1 403 Forbidden');
        die();
    }

    function sendMessage($chatId, $message, $logfh = null)
    {
        if(!($logfh === null))
            fwrite($logfh, 'Attempting to message '.$chatId.' with '.$message.PHP_EOL);
        $url = TG_BOTAPI_URL.'/sendMessage?chat_id='.$chatId.'&text='.urlencode(Encoding::fixUTF8(utf8_encode($message)));
        $res = file_get_contents($url);
        if(!($logfh === null))
            fwrite($logfh, 'Result was '.$res.PHP_EOL);
    }

    function getRandomResponse()
    {
        $responses = explode("\n", file_get_contents('responses.txt'));
        $keyrange = count($responses)-1;
        $randIdx = random_int(0, $keyrange);
        return $responses[$randIdx];
    }

    function getBeerResponse() {
        $responses = explode("\n", file_get_contents('beer_responses.txt'));
        $keyrange = count($responses)-1;
        $randIdx = random_int(0, $keyrange);
        return $responses[$randIdx];
    }

  
    $fh = fopen('../webhookpost.log', 'a');
    if(!$fh)
        trigger_error('Unable to open webhookpost.log for appending', E_USER_ERROR);
    $logFh = fopen('../debug.log', 'a');
    if(!$logFh)
        trigger_error('Unable to open ../debug.log for appending', E_USER_ERROR);
    fwrite($fh, date('[d.m.Y H:i:s] ').'Webhook triggered.'.PHP_EOL);
    fwrite($logFh, date('[d.m.Y H:i:s] ').'Webhook triggered.'.PHP_EOL);
    $reqJson = file_get_contents('php://input');
    $reqData = json_decode($reqJson, true);
    fwrite($fh, print_r($reqData, true));
    
    $msgRcvd = $reqData['message']['text'];
    $chatID  = $reqData['message']['chat']['id'];
    $botName = 'kuhamotibot';
    $lowerMsgRcvd = strtolower($msgRcvd);

    # We like beer, don't we?
    $fromStr = 'this week friday 15:00';
    $toStr = 'this week sunday 05:00';
    $fromTs = strtotime($fromStr);
    $toTs = strtotime($toStr);

    $time = time();
    $beerTime = $time >= $fromTs && $time <= $toTs;

    $lowerSplitted = explode(' ', $msgRcvd);
    if($lowerMsgRcvd == '/moti' || $lowerMsgRcvd == '/moti@'.$botName)
        sendMessage($chatID, ($beerTime ? getBeerResponse() : getRandomResponse()), $logFh);
    else if($lowerMsgRcvd == '/start' || $lowerMsgRcvd == '/start@'.$botName)
        sendMessage($chatID, 'My job is to bring motivation to your life. Please send me a /moti'.PHP_EOL, $logFh);
    else if($lowerMsgRcvd == '/sauce' || $lowerMsgRcvd == '/sauce@'.$botName)
        sendMessage($chatID, 'https://bitbucket.org/VonLatvala/kuhamotibot/src'.PHP_EOL, $logFh);
    else if($lowerMsgRcvd == '/credits' || $lowerMsgRcvd == '/credits@'.$botName)
        sendMessage($chatID, 'Phrases: author from http://lannistajakuha.com'.PHP_EOL.'Bot: Axel Latvala (@VonLatvala https://alatvala.fi)', $logFh);
    /*else 
        fwrite($fh, "Ei tajuu: ".$msgRcvd.PHP_EOL);*/
    
    fclose($fh);
    fclose($logFh);
